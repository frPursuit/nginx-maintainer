import smtplib
from email.message import EmailMessage
from typing import Dict, Optional

from pursuitlib.utils import is_null_or_empty

from settings import SMTP_USE_SSL, SMTP_HOST, SMTP_PORT, SMTP_USER, SMTP_PASSWORD, EMAIL_FROM, EMAIL_TO, NGINX_NAME

MAINTAINER_LABEL = "Nginx Maintainer"
MITIGATED_LABEL = "MITIGATED"
CRITICAL_LABEL = "CRITICAL"


def send_email_to_server(subject: str, content: str, server: smtplib.SMTP):
    message = EmailMessage()
    message['Subject'] = subject
    message.set_content(content, subtype='html')

    if not is_null_or_empty(SMTP_USER) and not is_null_or_empty(SMTP_PASSWORD):
        server.login(SMTP_USER, SMTP_PASSWORD)

    server.send_message(message, EMAIL_FROM, EMAIL_TO)


def send_email(subject: str, content: str):
    if SMTP_USE_SSL:
        with smtplib.SMTP_SSL(SMTP_HOST, SMTP_PORT) as server:
            send_email_to_server(subject, content, server)
    else:
        with smtplib.SMTP(SMTP_HOST, SMTP_PORT) as server:
            send_email_to_server(subject, content, server)


def format_error(error: str) -> str:
    return error.replace('\n', "<br>\n")


def send_report_email(errors: Dict[Optional[str], str]):
    if None in errors:
        subject = f"[{MAINTAINER_LABEL}/{CRITICAL_LABEL}] {NGINX_NAME} needs to be manually fixed !"
    elif len(errors.keys()) == 1:
        hostname = list(errors.keys())[0]
        subject = f"[{MAINTAINER_LABEL}/{MITIGATED_LABEL}] '{hostname}' has been disabled to avoid a global failure of {NGINX_NAME}"
    else: subject = f"[{MAINTAINER_LABEL}/{MITIGATED_LABEL}] Multiple hosts have been disabled to avoid a global failure of {NGINX_NAME}"

    content = ""

    if None in errors:
        content += "<b>The following error needs to be fixed manually:</b><br>\n<br>\n"
        content += format_error(errors[None])
        content += "<br>\n<br>\n"

    for host, error in errors.items():
        if host is None:
            continue
        content += f"<b>'{host}' has been disabled due to the following error:</b><br>\n<br>\n"
        content += format_error(error)
        content += "<br>\n<br>\n"

    send_email(subject, content)
