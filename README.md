# Nginx Maintainer

This program tries to maintain a nginx service operationnal by disabling faulty configuration files.

It also supports alerting by email.

## Deploy the maintainer in production

An Ansible playbook is provided in `/deploy` to help deploy the maintainer in production.

By default, it is installed in `/opt/nginx_maintainer`.

After running the playbook:

- Configure the applictaion by creating a `.env` file from `.env.example`
- Enable the HTTP Monitor timer: `systemctl enable nginxmtn.timer`
- Start the HTTP Monitor timer: `systemctl start nginxmtn.timer`
