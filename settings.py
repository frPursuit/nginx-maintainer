# Application settings
from pursuitlib.utils import get_env

APP_NAME = "nginx_maintainer"

VERSION = "0.1.0"
DEV_STAGE = "BETA"
DISPLAY_VERSION = VERSION if DEV_STAGE is None else VERSION + " " + DEV_STAGE

NGINX_NAME = get_env("NGINX_NAME", "Nginx")
NGINX_PATH = get_env("NGINX_PATH", "/usr/sbin/nginx")
CRITICAL_STATE_FILE = get_env("CRITICAL_STATE_FILE", "critical.state")

# Email settings

SMTP_HOST = get_env("SMTP_HOST")
SMTP_USE_SSL = get_env("SMTP_USE_SSL", "False").lower() == "true"
SMTP_PORT = get_env("SMTP_PORT", 465 if SMTP_USE_SSL else 25)
SMTP_USER = get_env("SMTP_USER", "")
SMTP_PASSWORD = get_env("SMTP_PASSWORD", "")

EMAIL_FROM = get_env("EMAIL_FROM")
EMAIL_TO = get_env("EMAIL_TO")
