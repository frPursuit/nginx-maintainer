import os.path
import re
import shutil
import sys
import subprocess
from typing import Optional, Tuple, List

import mail
from settings import NGINX_PATH, CRITICAL_STATE_FILE, APP_NAME, DISPLAY_VERSION

FAIL_SOURCE_PATTERN = re.compile(r"in (?P<filename>/.*):\d*")


def find_errors() -> List[Tuple[Optional[str], str]]:
    process = subprocess.run([NGINX_PATH, "-t"], capture_output=True)
    output = process.stderr.decode().split('\n')

    errors = []
    for line in output:
        match = FAIL_SOURCE_PATTERN.search(line)
        if match is not None:
            errors.append((str(match.group("filename")), line))

    if process.returncode != 0 and len(errors) == 0:  # If the error could not be traced back to a file
        errors.append((None, '\n'.join(output)))

    return errors


def main() -> int:
    print(f"Starting {APP_NAME} version {DISPLAY_VERSION}...")

    critical = False
    report = {}

    while True:  # Loop as long as new errors are found
        errors = find_errors()
        if len(errors) == 0:  # If there are no errors anymore, stop here
            break

        for file, error in errors:
            if file is not None:  # If the source of the error has been identified, neutralize it
                print(f"Error found on '{file}'")
                if os.path.isfile(file):
                    shutil.move(file, f"{file}.broken")
            else:  # Else, mark the error as critical (ie: a human action is required)
                print("Critical error found!")
                critical = True
            if file not in report:
                report[file] = error
            else: report[file] += f"\n{error}"

        if critical:  # If a critical error has been found, it cannot be automatically fixed: stop here
            break

    if not critical and os.path.isfile(CRITICAL_STATE_FILE):
        os.remove(CRITICAL_STATE_FILE)  # If the critical state file exists, remove it if it is no longer necessary

    if len(report) > 0 and not os.path.isfile(CRITICAL_STATE_FILE):  # Send a report if necessary
        print("Sending report email...")
        mail.send_report_email(report)

    if critical and not os.path.isfile(CRITICAL_STATE_FILE):  # Mark the state of nginx as critical
        open(CRITICAL_STATE_FILE, "w").close()  # Create an empty file

    if len(report) == 0:
        print("No errors found")

    return 0


if __name__ == '__main__':
    sys.exit(main())
